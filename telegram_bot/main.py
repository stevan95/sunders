from telegram.ext import Updater, InlineQueryHandler, CommandHandler
import requests
import re

def bop(bot, update):
    chat_id = update.message.chat_id
    bot.send_message(chat_id=chat_id, text="I'm sorry Dave I'm afraid I can't do that.")

def website(bot, update):
    chat_id = update.message.chat_id
    bot.send_message(chat_id=chat_id, text="https://hiljade.kamera.rs/")

def help(bot, update):
    chat_id = update.message.chat_id
    bot.send_message(chat_id=chat_id, text="List of commands: /website - vraca link ka sajtu")

def location(bot, update, args):
    user_says = " ".join(args)
    chat_id = update.message.chat_id
    #bot.send_message(chat_id=chat_id, text="Ok!")
    print(user_says)

def reqloc(bot, update):
    chat_id = update.message.chat_id
    location_keyboard = telegram.KeyboardButton(text="send_location", request_location=True)
    reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard)
    bot.send_message(chat_id=chat_id, text="Would you mind sharing your location and contact with me?", reply_markup=reply_markup)

def main():
    updater = Updater('1135372293:AAGyERbzoqaRud0LrWfKbVyG-qtC4mAfJuQ')
    dp = updater.dispatcher
    dp.add_handler(CommandHandler('bop', bop))
    dp.add_handler(CommandHandler('website', website))
    dp.add_handler(CommandHandler('help', help))
    dp.add_handler(CommandHandler('location', location, pass_args=True))
    updater.start_polling()
    updater.idle()

if __name__ == '__main__':
    main()
